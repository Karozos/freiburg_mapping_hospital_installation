echo 'Unpivoting hbp.csv'
java -jar MIPMapReduced.jar -unpivot ../../../data/hbp.csv ./postgresdb.properties "attribute" ./selectedHBP.txt -u ./unpivotHBP.txt
echo 'Done Unpivoting.'
echo 'Generating patient_num and encounter_num'
java -jar MIPMapReduced.jar -generate_id patientmapping.properties
java -jar MIPMapReduced.jar -generate_id encountermapping.properties
echo 'Done with patient_num and encounter_num'

echo 'Running first mapping...'
java -jar MIPMapReduced.jar /home/kostis/freiburg_mapping_installation/map.xml ./postgresdb.properties -db ./exporttoi2b2beforeharmonization.properties

echo 'Done with the first mapping!'

java -jar MIPMapReduced.jar /home/kostis/freiburg_mapping_installation/mapHarmonize.xml ./postgresdb.properties -db ./exporttoi2b2harmonized.properties

echo 'Done also with the Harmonization mapping!!'

java -jar MIPMapReduced.jar -runsql /home/kostis/freiburg_mapping_installation/pivot_i2b2_cde_SF_MinDate_NEW111.sql /home/kostis/freiburg_mapping_installation/exporttoi2b2harmonized.properties

echo 'Done pivoting... Made the flat csv... All good...'
	
